declare 
	@tblquery   AS VARCHAR(254)  	= 'subscr.NO_VAL_ACTV_PROD_ATTR_VW', 
	@on_rows  	AS VARCHAR(1200) 	= 'SUBSCR_SYS_NAME, PROD_ID, ADMIN_SYS_NAME,XREF_PROD_ID, XREF_PROD_HASH_KEY',
	@on_cols  	AS VARCHAR(1200)  	= 'SUBSCR_FILE_FIELD_NAME',
	@agg_func 	AS VARCHAR(25) 		= 'MAX' /* aggregate function you want to use */,
    @agg_col  	AS VARCHAR(254) 	= 'PROD_ATTR_VAL',@subscrsource AS VARCHAR(254)  = 'rs_dw.subscr_sys', @subscrsysname AS VARCHAR(254) = 'EDR Subscription - Set 1',
    @intotable_schema AS VARCHAR(254) = 'SUBSCR',
	@intotable 	AS VARCHAR(254) 	= 'SUBSCR_PROD_ATTR',
	@RetCode 	int, 
	@RetMessage varchar(254)

EXEC subscr.esp_build_subscr
		@tblquery,
		@on_rows,
		@on_cols,
		@agg_func,
		@agg_col,
		@subscrsource,
		@subscrsysname,
		@intotable_schema,
		@intotable,
		@RetCode 	OUTPUT,
		@RetMessage OUTPUT

Select  @RetCode 	OUTPUT,
        @RetMessage OUTPUT


select * from sys.tables where name ='SUBSCR_PROD_ATTR';
select * from sys.views where name ='SUBSCR_PROD_ATTR_VW';

select * from subscr.SUBSCR_PROD_ATTR;
select * from SUBSCR.SUBSCR_PROD_ATTR_VW;




Procedure failed; error line: 123; error number: 245; error message: Conversion failed when converting the varchar value '(@RedemptionFeeIndicator ' to data type int.

***** 