USE [RS_OperationalDataStore]
go

-- Drop Constraint, Rename and Create Table SQL

CREATE TABLE ods.PLAN_REF
(
    PLAN_REF_GEN_ID bigint       IDENTITY,
    PLAN_NBR        varchar(6)    NOT NULL,
    INS_TS          datetime     DEFAULT (getdate())  NOT NULL,
    LAST_UPDT_TS    datetime     NULL,
    LAST_UPDT_ID    varchar(64)   NOT NULL,
    CONSTRAINT PK_PLAN
    PRIMARY KEY CLUSTERED (PLAN_REF_GEN_ID),
    CONSTRAINT AK_PLAN
    UNIQUE NONCLUSTERED (PLAN_NBR)
)
go
