--@altsql  	NVARCHAR(max),		
--@altsqlupdt NVARCHAR(max),		
--@altsql1  	NVARCHAR(max),

--AND TABLE_SCHEMA = @intotable_schema

USE [RS_DW]
GO

/****** Object:  StoredProcedure [subscr].[eSP_Populate_Subscr_Rltnshp]    Script Date: 6/13/2018 1:59:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [subscr].[eSP_Populate_Subscr_Rltnshp]  
		@tblquery AS VARCHAR(254)  /*input table for pivot column */,
		@on_rows  AS VARCHAR(1200) /*grouping columns */,
		@on_cols  AS VARCHAR(1200) /*rotation column */,
		@agg_func AS VARCHAR(25) = 'MAX' /* aggregate function you want to use */,
		@agg_col  AS VARCHAR(254) /*column your aggregate will be applied to */,
		@subscrsource AS VARCHAR(254) /* subscription source table name */,
		@subscrsysname AS VARCHAR(254) /* subscription name */,
		@intotable_schema AS VARCHAR(254) /* new table schema*/,
		@intotable AS VARCHAR(254) /* new table that got created to house subscription attribute values */,
		@attribute_subject_area AS VARCHAR(20)  /*view name for attributes for this subject area */,
		@sub_run_time as datetime, /*date in which the effective date for the attribute values should be based upon */
		@sbjct_area_rltnshp_ind_col_name AS VARCHAR(254), /*used in rltnshp build only, name of rltnshp indicator col - will differ with every subject area */
		@RltnshpTblName AS VARCHAR(254)  /*input table for 2nd query */,
		@RltnshpIdCol AS VARCHAR(254)  /*Name of Relationship ID column */,
		@main_sbjct_area_id AS VARCHAR(20) /* The column name of the main subject area in the relationship.  this is not the relationship subject area id */,
		@main_sbjct_area_attr_view AS VARCHAR(254)  /*view that will return attributes for main subject area in relationship */,
		@RetCode int OUTPUT,
		@RetMessage varchar(254) OUTPUT
AS
BEGIN
/**************************************************************
** Name: subscr.esp_Populate_Subscr_Rltnshp_v2
** Desc: this proc will dynamically populate out the relationship subject area subscription output
** Auth: Marcie Rimmele
** Date: 06-07-2016
**************************************************************
** Change History
**************************
** Date         Author          Description
** --------     -------   ------------------------------------
** 02-16-2018   Vivek W		While deriving columns from metadata, table_schema was not provided .Added condition for the same.
** 02-16-2018	Vivek W		After executing all Inserts added logic to add Last_UPDT_TS column.
**************************************************************/

DECLARE

  @query   NVARCHAR(MAX),
  @newline VARCHAR(2),
  @ParmDefinition nvarchar(500),
  @insertddl     NVARCHAR(MAX),
   @viewddl     NVARCHAR(MAX),
  @ProcName varchar(120),
  @initTranCount int,
  @UserId char(16),
  @UpdateDate datetime,
  @Message_Parms varchar(4800),
  @imax int,
  @loopint int,
  @column_name NVARCHAR(max),
  @text_column_name NVARCHAR(max),
  @total_column_name NVARCHAR(max),
  @TOTAL_TEXT_COLUMN_NAME  NVARCHAR(max),
  @altsql  NVARCHAR(max),
  @altsqlupdt  NVARCHAR(max),
  @altsql1  NVARCHAR(max),
  @ORDER_SUBSCR_FILE_FIELD_NAME varchar(254)

  
SET NOCOUNT ON

SET @ProcName = 'subscr.esp_Populate_Subscr_Rltnshp'
BEGIN TRANSACTION @ProcName

/*
Initialize return code and message to "successful"
*/
SET @RetCode = 0
SET @RetMessage = 'subscr.esp_Populate_Subscr_Rltnshp Successful'

SET @UserId = dbo.eFN_Get_User_Id()
SET @UpdateDate = getdate()

SET @ParmDefinition = N'@in_subscr_sys_name varchar(254), @in_sub_run_time datetime, @in_sbjct_area_rltnshp_ind varchar(254)   '
SET @newline = CHAR(13) + CHAR(10);
SET @total_column_name = ' '
SET @total_text_column_name = ' '
SET @ORDER_SUBSCR_FILE_FIELD_NAME = ' '

BEGIN TRY



/* If input is a not a valid table or view, error. */
      IF COALESCE (OBJECT_ID(@tblquery, N'U'),
         OBJECT_ID(@tblquery, N'V')) IS  NULL
      BEGIN
        SET @RetMessage = 'Input table/view does not exist'
        RAISERROR (@RetMessage,16,1)
      END
/* If table to populate is a not a valid table, error */
      IF  OBJECT_ID(@intotable_schema  +'.'+@intotable, N'U') IS  NULL
      BEGIN
        SET @RetMessage = 'Table to populate does not exist'
        RAISERROR (@RetMessage,16,1)
      END


BEGIN TRY
/* Added TABLE_SCHEMA condition to avoid discrepency while accessing columns from metadata */
/* Get the list of columns that we need to insert to */
        BEGIN

                SELECT 	@imax = count(column_name)
                FROM 	INFORMATION_SCHEMA.COLUMNS
				WHERE 	TABLE_NAME = @intotable
				AND 	TABLE_SCHEMA = @intotable_schema

                SET @loopint = 1
                WHILE (@loopint <= (@imax -1))
                BEGIN
                
				SELECT  @COLUMN_NAME = COLUMN_NAME + ' ,'
				FROM 	INFORMATION_SCHEMA.COLUMNS
                WHERE 	TABLE_NAME = @intotable
				AND 	TABLE_SCHEMA = @intotable_schema
                AND 	ORDINAL_POSITION = @loopint
                ORDER BY ORDINAL_POSITION

 SET @TOTAL_COLUMN_NAME = @TOTAL_COLUMN_NAME   + @COLUMN_NAME
                        SET  @loopint = @loopint +1

                END

             SELECT  @COLUMN_NAME = COLUMN_NAME
                FROM INFORMATION_SCHEMA.COLUMNS
                WHERE 	TABLE_NAME = @intotable
				AND 	TABLE_SCHEMA = @intotable_schema
                AND 	ORDINAL_POSITION = @imax
                ORDER BY ORDINAL_POSITION

                SET @TOTAL_COLUMN_NAME = @TOTAL_COLUMN_NAME + @COLUMN_NAME

        END

BEGIN TRY

       BEGIN
/* If input is a valid table or view, construct a SELECT statement against it. */
        IF COALESCE(OBJECT_ID(@tblquery, N'U'),
           OBJECT_ID(@tblquery, N'V')) IS NOT NULL
                SET @query = 'SELECT  * FROM ' + @tblquery  ;

/* Make the query a derived table. */
                SET @query = N'( ' + @query + @newline + N'      ) AS Query';

        END
BEGIN TRY
       BEGIN
             SET @imax = 0
/* Get the list of attributes for the subject area that we need to include in our pivot statement.  The pivot statement requires the attribute
names have a bracket "[]" around the attribute name */

             SELECT @imax = count(SUBSCR_FILE_FIELD_NAME)
               FROM dbo.ENTPRS_SBJCT_AREA_ATTR_VW
              WHERE SBJCT_AREA_NAME = @attribute_subject_area


             SET @loopint = 1
                WHILE (@loopint <= (@imax -1))
             BEGIN
/* Construct column list. */
               SELECT
                  top 1 @TEXT_COLUMN_NAME = '['+SUBSCR_FILE_FIELD_NAME+'] , '
                       ,@ORDER_SUBSCR_FILE_FIELD_NAME = SUBSCR_FILE_FIELD_NAME
                  FROM dbo.ENTPRS_SBJCT_AREA_ATTR_VW
                  WHERE SBJCT_AREA_NAME = @attribute_subject_area
                  AND SUBSCR_FILE_FIELD_NAME >@ORDER_SUBSCR_FILE_FIELD_NAME
                  ORDER BY SUBSCR_FILE_FIELD_NAME ASC

                  SET @TOTAL_TEXT_COLUMN_NAME = @TOTAL_TEXT_COLUMN_NAME + @TEXT_COLUMN_NAME
                  SET  @loopint = @loopint +1


              END

             SELECT  top 1 @TEXT_COLUMN_NAME = '['+SUBSCR_FILE_FIELD_NAME+']'
               FROM dbo.ENTPRS_SBJCT_AREA_ATTR_VW
               WHERE SBJCT_AREA_NAME = @attribute_subject_area
               ORDER BY SUBSCR_FILE_FIELD_NAME DESC

                SET @TOTAL_TEXT_COLUMN_NAME = @TOTAL_TEXT_COLUMN_NAME + @TEXT_COLUMN_NAME

        END

/* Create the Insert statement, will insert all relationships with active attributes */
   BEGIN


          SET @insertddL =
         N'INSERT INTO ' + @intotable_schema+'.'+@intotable      + @newline +
        N'  ( ' +     @TOTAL_COLUMN_NAME             + ' )'           + @newline +
        N' SELECT ' +   @TOTAL_COLUMN_NAME+ @newline +
        N' FROM  ' +@newline +
        N'  ( SELECT '               + @newline +
        N'      ' + @on_rows + N','                          + @newline +
        N'      ' + @on_cols + N' AS pivot_col,'             + @newline +
        N'      ' + @agg_col + N' AS agg_col'                + @newline +
        N'    FROM '                                         + @newline +
        N'      ' + @tblquery  +' CROSS APPLY ' + @subscrsource + @newline +

        N' WHERE (((EFF_DT <=  @in_sub_run_time ) AND' +
        N' (END_DT >=  @in_sub_run_time )) OR' +
        N'  @in_sbjct_area_rltnshp_ind = ''Y'')  AND' +
        N' ' + @main_sbjct_area_id + ' in ( SELECT ' +  @main_sbjct_area_id + ' from ' + @main_sbjct_area_attr_view + ' ) AND ' +
        N' subscr_sys_name = @in_subscr_sys_name   '                   + @newline +
        N'  ) AS PivotInput'                                  + @newline  +
        N'  PIVOT'                                           + @newline +
        N'    ( ' + @agg_func + N'(agg_col)'                 + @newline +
        N'      FOR pivot_col '                               + @newline +
        N'        IN(' + @TOTAL_TEXT_COLUMN_NAME + N')'    + @newline +
        N'    ) AS PivotOutput;'


/* Execute the Insert statement */
       EXEC sp_executesql @insertddl OUTPUT,  @parmdefinition, @in_subscr_sys_name = @subscrsysname
      , @in_sub_run_time =@sub_run_time, @in_sbjct_area_rltnshp_ind = @sbjct_area_rltnshp_ind_col_name



        END


/* Create the 2nd Insert statement, will insert all active relationships with end dated attributes and no attributes */
      BEGIN


        SET @insertddL =
        N' INSERT INTO ' + @intotable_schema+'.'+@intotable      + @newline +
        N'  ( ' +     @on_rows            + ' )'           + @newline +
        N' SELECT ' +   @on_rows                           + @newline +
        N'    FROM '                                         + @newline +
        N'      ' + @RltnshpTblName  +' CROSS APPLY ' + @subscrsource + @newline +
        N' WHERE ' + @sbjct_area_rltnshp_ind_col_name + ' = ''Y''  AND ' + @newline +
        N' ' + @main_sbjct_area_id + ' in ( SELECT ' +  @main_sbjct_area_id + ' from ' + @main_sbjct_area_attr_view + ' ) AND ' +
        N' subscr_sys_name =  @in_subscr_sys_name ' + @newline +
        N' AND ' + @RltnshpIdCol + ' not in ( SELECT ' + @RltnshpIdCol + @newline +
        N'                              FROM ' + @tblquery + @newline +
        N'                              WHERE END_DT > =  @in_sub_run_time )' + @newline

/* Execute the Insert statement */
       EXEC sp_executesql @insertddl OUTPUT,  @parmdefinition, @in_subscr_sys_name = @subscrsysname
      , @in_sub_run_time =@sub_run_time, @in_sbjct_area_rltnshp_ind = @sbjct_area_rltnshp_ind_col_name


/*
 SET @altsql =  'ALTER TABLE '+ @intotable_schema+'.'+@intotable +' ADD LAST_UPDT_TS DATETIME ' ;
	 exec ( @altsql );
	 
	 SET @altsqlupdt = 'UPDATE ' + @intotable_schema+'.'+@intotable +' SET LAST_UPDT_TS = '''+ convert(varchar,getdate(),21) + '''';
	 exec(@altsqlupdt );
	 
	 SET @altsql1 = 'ALTER TABLE '+ @intotable_schema+'.'+@intotable +' ALTER COLUMN LAST_UPDT_TS DATETIME NOT NULL ' ;
	 exec ( @altsql1 );
*/

   END

END TRY
/* Error with Insert EXEC SQL */
BEGIN CATCH
        SET @Message_Parms = @insertddL
        SET @RetCode = -30
SET @RetMessage = ERROR_MESSAGE()
END CATCH

END TRY
/* Error with table validity check SQL */
BEGIN CATCH
        SET @Message_Parms = @tblquery
        SET @RetCode = -25
        SET @RetMessage = ERROR_MESSAGE()
END CATCH

END TRY
/* Error getting the column list */
BEGIN CATCH
        SET @Message_Parms = @TOTAL_COLUMN_NAME
        SET @RetCode = -15
        SET @RetMessage = ERROR_MESSAGE()
END CATCH

END TRY
/* Invalid Input */
BEGIN CATCH
        SET @Message_Parms = NULL
        SET @RetCode = -10
        SET @RetMessage = ERROR_MESSAGE()
END CATCH

/*
Determine to commit or rollback transaction
*/
    IF @RetCode >= 0

      COMMIT TRANSACTION @ProcName

   ELSE
      BEGIN
      IF @@TRANCOUNT > 0

         ROLLBACK TRANSACTION

      END

/*
Write out to Message Log
*/

   EXEC dbo.eSP_Write_Message_Log @RetCode,  @UpdateDate, @procname, @Message_Parms, @RetMessage
END

GO

/****************************************************************************************************************************
****************************************************************************************************************************/

USE [RS_DW]
GO

/****** Object:  StoredProcedure [subscr].[eSP_Build_Subscr]    Script Date: 6/13/2018 1:57:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [subscr].[eSP_Build_Subscr]     
		@tblquery    AS VARCHAR(254)  /*input table for pivot column */,
		@on_rows     AS VARCHAR(1200) /*grouping columns */,
		@on_cols     AS VARCHAR(1200) /*rotation column */,
		@agg_func    AS VARCHAR(25) = 'MAX' /* aggregate function you want to use */,
		@agg_col     AS VARCHAR(254) /*column your aggregate will be applied to */,
		@subscrsource AS VARCHAR(254) /* subscription source table name */,
		@subscrsysname AS VARCHAR(254) /* subscription name */,
		@intotable_schema AS VARCHAR(254) /* new table schema*/,
		@intotable AS VARCHAR(254) /* new table that will get create housing subscription attribute values */,
		@RetCode int OUTPUT,
		@RetMessage varchar(254) OUTPUT
AS
BEGIN
/**************************************************************
** Name: subscr.esp_Build_Subscr
** Desc: this proc will dynamically create the subject area subscription table and view.
** Auth: Marcie Rimmele
** Date: 03-01-2016
**************************************************************
** Change History
**************************
** Date         Author          Description
** --------     -------   ------------------------------------
**
**************************************************************/

DECLARE
  @sql     NVARCHAR(MAX),
  @query   NVARCHAR(MAX),
  @cols    VARCHAR(MAX),
  @newline VARCHAR(2),
  @ParmDefinition nvarchar(500),
  @tblsql      NVARCHAR(MAX),
  @viewsql     NVARCHAR(MAX),
  @viewname    NVARCHAR(200),
  @viewddl     NVARCHAR(MAX),
  @ProcName varchar(120),
  @initTranCount int,
  @UserId char(16),
  @UpdateDate datetime,
  @Message_Parms varchar(1200),
  @imax int,
  @loopint int,
  @column_name NVARCHAR(max),
  @total_column_name NVARCHAR(max),
  @icount int,
  @intotabletotal NVARCHAR(max)

SET NOCOUNT ON

SET @ProcName = 'subscr.esp_Build_Subscr'
BEGIN TRANSACTION @ProcName

/*
Initialize return code and message to "successful"
*/
SET @RetCode = 0
SET @RetMessage = 'subscr.esp_Build_Subscr Successful'

SET @UserId = [dbo].eFN_Get_User_Id()
SET @UpdateDate = getdate()

SET @ParmDefinition = N'@in_subscr_sys_name varchar(254)'
SET @newline = CHAR(13) + CHAR(10);
SET @total_column_name = ' '

BEGIN TRY
/* proc will only allow new entities in the subscr schema to be created */

      IF @intotable_schema != 'SUBSCR'
      BEGIN
        SET @RetMessage = 'Can not drop a table/view that is not part of SUBSCR schema!!'
        RAISERROR (@RetMessage,16,1)
      END
/* If input is a not a valid table or view, error. */
      IF COALESCE(OBJECT_ID(@tblquery, N'U'),
         OBJECT_ID(@tblquery, N'V')) IS  NULL
      BEGIN
        SET @RetMessage = 'Input table/view does not exist'
        RAISERROR (@RetMessage,16,1)
      END

BEGIN TRY
      BEGIN
/* set up SQL to drop table and view if already existing */
        SET @tblSQL = 'IF OBJECT_ID(N''' + @intotable_schema +'.'+@intotable+ ''',N''U'') IS NOT NULL DROP TABLE ' + @intotable_schema +'.'+ @intotable
        SET @viewname = @intotable_schema+ '.'+ @intotable + '_VW'
        SET @viewSQL = 'IF OBJECT_ID(N''' + @Viewname+ ''',N''V'') IS NOT NULL DROP VIEW ' + @viewname

        EXEC (@tblSQL);
        EXEC (@viewSQL);
      END

      BEGIN
/* If input is a valid table or view, construct a SELECT statement against it. */
        IF COALESCE(OBJECT_ID(@tblquery, N'U'),
           OBJECT_ID(@tblquery, N'V')) IS NOT NULL
           SET @query = 'SELECT  * FROM ' + @tblquery  ;

/* Make the query a derived table. */
                SET @query = N'( ' + @query + @newline + N'      ) AS Query';
      END

BEGIN TRY
      BEGIN
/* Construct column list. */
        SET @sql =
        N'SET @result = '                                    + @newline +
        N'  STUFF('                                          + @newline +
        N'    (SELECT N'','' + '
             + N'QUOTENAME(pivot_col) AS [text()]'       + @newline +
        N'     FROM (SELECT DISTINCT('
           + @on_cols + N') AS pivot_col'              + @newline +
        N'           FROM' + @query + N') AS DistinctCols'   + @newline +
        N'     ORDER BY pivot_col'                           + @newline +
        N'     FOR XML PATH('''')),'                         + @newline +
        N'    1, 1, N'''');'

        EXEC sp_executesql
        @stmt   = @sql,
        @params = N'@result AS NVARCHAR(MAX) OUTPUT',
        @result = @cols OUTPUT;

     END

     BEGIN
/* Create the PIVOT query. */
       SET @sql =
        N'SELECT ' + @on_rows    +', ' +@COLS                + @newline +
        N'INTO ' +@intotable_schema +'.'+@intotable                                 + @newline +
        N'FROM'                                              + @newline +
        N'  ( SELECT '               + @newline +
        N'      ' + @on_rows + N','                          + @newline +
        N'      ' + @on_cols + N' AS pivot_col,'             + @newline +
        N'      ' + @agg_col + N' AS agg_col'                + @newline +
        N'    FROM '                                         + @newline +
        N'      ' + @query  +' CROSS APPLY ' + @subscrsource + @newline +
        ' WHERE EFF_DT <=  getdate() AND' +
         ' subscr_sys_name = @in_subscr_sys_name   '                   + @newline +
        N'  ) AS PivotInput'                                 + @newline +
        N'  PIVOT'                                           + @newline +
        N'    ( ' + @agg_func + N'(agg_col)'                 + @newline +
        N'      FOR pivot_col'                               + @newline +
        N'        IN(' + @cols + N')'                        + @newline +
        N'    ) AS PivotOutput;'

        EXEC sp_executesql @sql OUTPUT,  @parmdefinition,  @in_subscr_sys_name = @subscrsysname

     END

BEGIN TRY
/* Get column list of table just created.  This is what will be used to create the new view */
     BEGIN
       SELECT 	@imax = count(column_name)
         FROM 	INFORMATION_SCHEMA.COLUMNS
        WHERE 	TABLE_NAME = @intotable
		AND 	TABLE_SCHEMA = @intotable_schema

        SET @loopint = 1
        WHILE (@loopint <= (@imax -1))
/* Select the column name + an ending ','; we'll handle the select of the last column separately in order to not have a comma after the last column */
        BEGIN
          SELECT  @COLUMN_NAME = COLUMN_NAME + ' ,'
            FROM INFORMATION_SCHEMA.COLUMNS
           WHERE TABLE_NAME = @intotable
		   AND 	TABLE_SCHEMA = @intotable_schema
           AND 	ORDINAL_POSITION = @loopint
           ORDER BY ORDINAL_POSITION

          SET @TOTAL_COLUMN_NAME = @TOTAL_COLUMN_NAME   + @COLUMN_NAME
          SET  @loopint = @loopint +1

        END
/* Select the last column name   */
        SELECT  @COLUMN_NAME = COLUMN_NAME
          FROM INFORMATION_SCHEMA.COLUMNS
         WHERE 	TABLE_NAME = @intotable
		 AND 	TABLE_SCHEMA = @intotable_schema
         AND 	ORDINAL_POSITION = @imax
         ORDER BY ORDINAL_POSITION

        SET @TOTAL_COLUMN_NAME = @TOTAL_COLUMN_NAME + @COLUMN_NAME


        SET @viewddL =
        N'CREATE VIEW ' + @viewname    +' AS'    + @newline +
        N'SELECT ' +     @TOTAL_COLUMN_NAME                        + @newline +
        N'FROM ' +   @intotable_schema+'.'+@intotable

        EXEC (@viewddl)

        END

END TRY
/* Error with view EXEC SQL */
BEGIN CATCH
        SET @Message_Parms = @viewddl
        SET @RetCode = -30
        SET @RetMessage = ERROR_MESSAGE()
END CATCH

END TRY
/* Error with table EXEC SQL */
BEGIN CATCH
        SET @Message_Parms = @sql
        SET @RetCode = -25
        SET @RetMessage = ERROR_MESSAGE()
END CATCH

END TRY
/* Error with table/view drop SQL */
BEGIN CATCH
        SET @Message_Parms = @tblSQL + @viewSQL
        SET @RetCode = -15
        SET @RetMessage = ERROR_MESSAGE()
END CATCH


END TRY
/* Error trying to create non-subscr schema table/view */
BEGIN CATCH
        SET @Message_Parms = 'Input table name: ' + @intotable
        SET @RetCode = -10
        SET @RetMessage = ERROR_MESSAGE()
END CATCH

/*
 Determine to commit or rollback transaction
*/
    IF @RetCode >= 0

      COMMIT TRANSACTION @ProcName

   ELSE
      BEGIN
      IF @@TRANCOUNT > 0

         ROLLBACK TRANSACTION

      END

/*
Write out to Message Log
*/

   EXEC [dbo].eSP_Write_Message_Log @RetCode,  @UpdateDate, @procname, @Message_Parms, @RetMessage
END

GO

/****************************************************************************************************************************
****************************************************************************************************************************/
USE [RS_DW]
GO

/****** Object:  UserDefinedFunction [dbo].[eFN_Format_Error_Message]    Script Date: 6/13/2018 1:57:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[eFN_Format_Error_Message] ()
RETURNS varchar(254)
AS
BEGIN
   DECLARE @MessageText varchar(254)
   SET @MessageText = 'Error: ' + CONVERT(varchar(50), ERROR_NUMBER()) +
      ', Severity: ' + CONVERT(varchar(5), ERROR_SEVERITY()) +
      ', State: ' + CONVERT(varchar(5), ERROR_STATE()) + 
      ', Procedure: ' + ISNULL(ERROR_PROCEDURE(), '-') +
      ', Line: ' + CONVERT(varchar(5), ERROR_LINE()) +
      ', Message: ' + ERROR_MESSAGE()
   RETURN @MessageText
END 

GO


